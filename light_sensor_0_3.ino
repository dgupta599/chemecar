#include <Wire.h>
#include <Math.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2561_U.h>

/*
  April 5, 2014, Copyright Colin Grambow

  Connections
  ===========
  Connect SCL to analog 5
  Connect SDA to analog 4
  Connect VDD to 3.3V DC
  Connect GROUND to common ground
  Connect white LED to pin 13 and across 56ohm resistor to ground
  Connect transistor base to pin 11
  Connect switch to pin 2 across 10kohm resistor to ground and to 5V
*/
   
Adafruit_TSL2561_Unified tsl = Adafruit_TSL2561_Unified(TSL2561_ADDR_FLOAT, 12345);
const int ledPin = 13;
const int transistorBase = 11;
const int switchPin = 2;
const int debounceDelay = 30;  // ms to wait until switch is stable, adjust according to switch used
double ln_initLum = 0.0;  // initial luminosity value (logarithm)

//-------------------CONSTANTS ADDED BY DANIEL-----------------
const int switchPowerPin = 4;
boolean switchState; // True when the button is pressed
boolean runState; // True when the Relay is conected
boolean endState; // True when the Relay just disconnected
//-------------------END OF CONSTANTS ADDED---------------------


void configureSensor(void)
{
  /* You can also manually set the gain or enable auto-gain support */
  tsl.setGain(TSL2561_GAIN_1X);      /* No gain ... use in bright light to avoid sensor saturation */
  // tsl.setGain(TSL2561_GAIN_16X);     /* 16x gain ... use in low light to boost sensitivity */
  // tsl.enableAutoGain(true);          /* Auto-gain ... switches automatically between 1x and 16x */
  
  /* Changing the integration time gives you better sensor resolution (402ms = 16-bit data) */
  // tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_13MS);      /* fast but low resolution */
  tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_101MS);  /* medium resolution and speed   */
  // tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_402MS);  /* 16-bit data but slowest conversions */
}


// debounce switch
//-----------------MODIFICATIONS BY DANIEL-------------------------
void debounce()
{
  if(!runState & !endState) //only do this sequence when runState is false
  {
    boolean state;
    boolean previousState;
  
    previousState = digitalRead(switchPin);  // store switch state
    for(int counter = 0; counter < debounceDelay; counter++)
    {
      delay(1);
      state = digitalRead(switchPin);
      if(state != previousState)
      {
        counter = 0;  // reset counter if switch changes
        previousState = state; // save current state
      }
    }
    switchState = true;
    
    // done when switch has been stable longer than the debounce period
    return;
  }
}
//------------------END OF MODIFICATION-------------------

void setup(void) 
{
  Serial.begin(9600);
  
  /* Initialise the sensor */
  if(!tsl.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.print("Ooops, no TSL2561 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }
  
  configureSensor();
  
  pinMode(ledPin,OUTPUT);
  pinMode(transistorBase, OUTPUT);
  pinMode(switchPin,INPUT);
  pinMode(switchPowerPin,OUTPUT);
  //  pinMode(powerPin,OUTPUT);
  
    // send 5V to LED
  digitalWrite(ledPin,HIGH);
  digitalWrite(transistorBase, LOW);
  digitalWrite(switchPin,LOW);
  digitalWrite(switchPowerPin,HIGH);
  //  digitalWrite(powerPin,HIGH);
  
  switchState = false;
  runState = false;
  endState =false;
  attachInterrupt(0,debounce,RISING);
}

void loop(void) 
{  
  // change in logarithm of luminosity
  double del_ln_lum = 0.2858222;
  
  sensors_event_t event;
  tsl.getEvent(&event);
 
  
  // start if switch is pressed
  //-------------------SEQUENCE MODIFIED BY DAN---------------//
  //State machine with3 different possibilities
  //switchState, runState, endState
  
  if (switchState)
  {
    //read initial luminosity value
    ln_initLum = log((double) event.light);
    digitalWrite(transistorBase,HIGH);
    switchState = false;
    runState = true;
  }
  
  
  if(runState)
  {
    //Procedure to stop the car
    if ((ln_initLum - log((double) event.light)) >= del_ln_lum)
    {
      digitalWrite(transistorBase, LOW);
      runState = false;
      endState = true;
    }
    
   if(endState)
   {
     delay(2000);
     endState = false;
   }
   
 }
//--------------------END OF MODIFICATION-------------------//

//  Analog write ( pin, value between 0 and 255 ) <- fade the LED from full on to full off, and decrease this value in 150 ms increments (total of 38 seconds)

//  digitalWrite(ledPin,HIGH);
//  sensors_event_t event;
//  tsl.getEvent(&event);

//  int data = (int) event.light;
//  Serial.print("C");
//  Serial.write(data>>8);
//  Serial.write(data&0xff);
//  delay(150);


//  Serial.print(initial luminosity value) <- This is the sensor
//  Serial.print(delta ln luminosity)
//  if (ln(initial luminosity value) - ln(luminosity value) >= delta ln_luminosity
//  {
//    digitalWrite(transistorBase,LOW);
//    Serial.print(luminosity value); <- We want this value to be equal to the value of (ln_initLum + del_ln_lum). This should also correlate with the excel models.
//  }
//
  delay(150);
}